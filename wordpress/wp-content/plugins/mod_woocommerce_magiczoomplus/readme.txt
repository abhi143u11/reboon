#######################################################

 Magic Zoom Plus™
 WooCommerce module version v6.6.16 [v1.6.53:v5.2.3]
 
 www.magictoolbox.com
 support@magictoolbox.com

 Copyright 2017 Magic Toolbox

#######################################################

INSTALLATION:

1. Upload the zip file via your WordPress admin area.

2. Activate Magic Zoom Plus plugin for WooCommerce in the Plugins menu of WordPress.

3. Magic Zoom Plus is ready to use!

